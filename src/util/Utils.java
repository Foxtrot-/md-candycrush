package util;

import java.util.Random;

import model.Couleur;

public class  Utils {
	
	public static Couleur getRandomColor() {
		Couleur [] couleurs = Couleur.values();
		
		Random r = new Random();
		int color = r.nextInt(couleurs.length-2);
		
		return couleurs[color];
	}
	
	public static boolean isMystere() {
		Random r = new Random();
		return  r.nextInt(100) == 0;
	}
	
	public static boolean isRayeHorizontal() {
		Random r = new Random();
		return  r.nextInt(100) == 0;
	}
	
	public static boolean isRayeVertical() {
		Random r = new Random();
		return  r.nextInt(100) == 0;
	}
	
	public static boolean isBombe() {
		Random r = new Random();
		return  r.nextInt(100) == 0;
	}
	
	public static boolean isMalus() {
		Random r = new Random();
		return  r.nextInt(100) == 0; 
	}
}
