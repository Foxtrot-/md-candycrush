package view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Panel;

import controller.JeuController;
import model.BonbonBasic;
import model.BonbonBombe;
import model.BonbonMalus;
import model.BonbonMystere;
import model.BonbonRayeHorizontal;
import model.BonbonRayeVertical;
import model.BonbonState;
import model.Couleur;

public class GraphicWorld extends Panel{
    
	// image pour le rendu hors écran
    private Image buffer;
    
    private int selectedX = -1, selectedY = -1; 
    private int swappedX = -1, swappedY = -1;
    JeuController jeu = new JeuController();

	
    // évite le syntillements
    public void update(Graphics g) {
        paint(g);
    }

    // routine d'affichage : on fait du double buffering
    public void paint(Graphics g2) {
    
    	
        if(buffer == null) buffer = createImage(800, 700);
        Graphics2D g = (Graphics2D) buffer.getGraphics();

        // fond
        g.setColor(Color.WHITE);
        g.fillRect(0, 0, getWidth(), getHeight());

        // afficher la grille vide
        g.setColor(Color.BLACK);
        for(int i = 0; i < 9; i++) {
            g.drawLine(32 * i, 0, 32 * i, 8 * 32 + 1); 
            g.drawLine(0, 32 * i, 8 * 32 + 1, 32 * i); 
        }

        // afficher la première case sélectionnée
        if(selectedX != -1 && selectedY != -1) {
            g.setColor(Color.ORANGE);
            g.fillRect(selectedX * 32 + 1, selectedY * 32 + 1, 31, 31);
        }

        // afficher la deuxième case sélectionnée
        if(swappedX != -1 && swappedY != -1) {
            g.setColor(Color.YELLOW);
            g.fillRect(swappedX * 32 + 1, swappedY * 32 + 1, 31, 31);
        }

        // afficher le contenu de la grille
        for(int i = 0; i < 8; i++) {
            for(int j = 0; j < 8; j++) {
                drawBonbon(g,i,j);
            }
        }
        
        g.setColor(Color.RED);
        //score
        g.drawString("Score = " + jeu.getScore().getPoints() , 200, 280);
        
        // copier l'image à l'écran
        g2.drawImage(buffer, 0, 0, null);
    }
    
    private void drawBonbon(Graphics2D g, int i, int j) {
    	g.setColor(Couleur.getColor(jeu.getColor(i, j)));
        BonbonState state = jeu.getState(i, j);
        
        if (state.getClass().equals(BonbonBasic.class)) {
        	g.fillOval(32 * i + 3, 32 * j + 3, 27, 27);
        } 
        else if (state.getClass().equals(BonbonBombe.class)) {
        	g.fillRect(32 * i + 3, 32 * j + 3, 27, 27);
        } 
        else if (state.getClass().equals(BonbonMalus.class)) {
        	g.fillOval(32 * i + 3, 32 * j + 3, 27, 27);
        	g.setColor(Color.BLACK);
        	g.fillOval(32 * i + 3 + 10, 32 * j + 3 + 10, 7, 7);
        } 
        else if (state.getClass().equals(BonbonMystere.class)) {
        	g.fillOval(32 * i + 3, 32 * j + 3, 27, 27);
        } 
        else if (state.getClass().equals(BonbonRayeHorizontal.class)) {
        	g.fillOval(32 * i + 3, 32 * j + 3 + 5, 27, 17);
        }
        else if (state.getClass().equals(BonbonRayeVertical.class)) {
        	g.fillOval(32 * i + 3 + 5, 32 * j + 3, 17, 27);
        }
        
        
		
	}

	// taille de la fenêtre
    public Dimension getPreferredSize() {
        return new Dimension(32 * 8 + 1 + 100, 32 * 8 + 1 + 100);
    }
    
    public boolean ValidateSelected ()
    {
    	if (selectedX != -1 && selectedY !=-1)
    		return true;
    	return false;
    }
    
    public boolean ValidateSwapped ()
    {
    	if (swappedX != -1 && swappedY !=-1)
    		return true;
    	return false;
    }
    
    public void setSelected (int x, int y)
    {
    	selectedX = x;
    	selectedY = y;
    }
    
    public void setSwapped (int x, int y)
    {
    	swappedX = x;
    	swappedY = y;
    }

	public int getSelectedX() {
		return selectedX;
	}

	public void setSelectedX(int selectedX) {
		this.selectedX = selectedX;
	}

	public int getSelectedY() {
		return selectedY;
	}

	public void setSelectedY(int selectedY) {
		this.selectedY = selectedY;
	}

	public int getSwappedX() {
		return swappedX;
	}

	public void setSwappedX(int swappedX) {
		this.swappedX = swappedX;
	}

	public int getSwappedY() {
		return swappedY;
	}

	public void setSwappedY(int swappedY) {
		this.swappedY = swappedY;
	}
    
    
}
