package view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Panel;

import controller.JeuController;
import model.Couleur;

public class MainView extends Panel{
	
	// image pour le rendu hors écran
    private Image buffer;
    
    private int selectedX = -1, selectedY = -1;
	
	 // évite le syntillements
    public void update(Graphics g) {
        paint(g);
    }

    // routine d'affichage : on fait du double buffering
    public void paint(Graphics g2 ) {
    
    	
        if(buffer == null) buffer = createImage(800, 600);
        Graphics2D g = (Graphics2D) buffer.getGraphics();

        // fond
        g.setColor(Color.DARK_GRAY);
        g.fillRect(0, 0, getWidth(), getHeight());

        g.setColor(Color.WHITE);
        g.fill3DRect(75, 100, 100, 50, false);
        
        g.setColor(Color.BLUE);
//        g.setFont(getFont().)
        
        g.drawString("JOUER", 105, 130);

        // copier l'image à l'écran
        g2.drawImage(buffer, 0, 0, null);
    }
    
    // taille de la fenêtre
    public Dimension getPreferredSize() {
        return new Dimension(32 * 8 + 1, 32 * 8 + 1);
    }
    
    public void setSelected (int x, int y)
    {
    	selectedX = x;
    	selectedY = y;
    }

}
