package controller;

import model.BonbonState;
import model.Couleur;
import model.Grille;
import model.Score;
import util.Utils;

public class JeuController{
	
	private  Grille grille = Grille.getInstance(); ;
	
	
	// détermine si l'échange entre deux cases est valide
	public  boolean isValidSwap(int x1, int y1, int x2, int y2) {

		// il faut que les cases soient dans la grille
		if(x1 < 0 || x2 < 0 || y1 < 0 || y2 < 0) 
			return false;

		// que les cases soient à côté l'une de l'autre
		if(Math.abs(x2 - x1) + Math.abs(y2 - y1) != 1) 
			return false;

		// et que les couleurs soient différentes
		if(grille.getBonbons()[x1][y1].getCouleur().equals(grille.getBonbons()[x2][y2].getCouleur())) 		
			return false;

		// alors on effectue l'échange
		grille.swap(x1, y1, x2, y2);

		// et on vérifie que ça créé un nouvel alignement
		boolean newAlignment = false;
		for(int i = 0; i < 3; i++) {
			newAlignment |= grille.horizontalAligned(x1 - i, y1);
			newAlignment |= grille.horizontalAligned(x2 - i, y2);
			newAlignment |= grille.verticalAligned(x1, y1 - i);
			newAlignment |= grille.verticalAligned(x2, y2 - i);
		}

		// puis on annule l'échange
		swap(x1, y1, x2, y2);
		return newAlignment;

	}
	
	public Couleur getColor(int i, int j)
	{
		return grille.getBonbons()[i][j].getCouleur();
	}
	
	public BonbonState getState(int i, int j)
	{
		return grille.getBonbons()[i][j].getState();
	}

	public  void swap(int selectedX, int selectedY, int swappedX, int swappedY) {
		grille.swap(selectedX, selectedY, swappedX, swappedY);		
	}
	
	public  boolean removeAlignments(){
		return grille.removeAlignments();
	}
	
    // remplir les cases vides par gravité, et générer des cases aléatoirement par le haut
    public  boolean fill() {
        boolean modified = false;
        for(int i = 0; i < grille.getWidth(); i++) {
            for(int j = grille.getHeight()-1; j >= 0; j--) {
            	//System.out.println(i+ " "+j);
                if(grille.getBonbons()[i][j].getCouleur().equals(Couleur.White)){
                    if(j == 0) {
                    	grille.newBonbon(i,j);
                    } else {
                    	grille.moveDown(i, j);
                    }
                    modified = true;
                }
            }
        }
        return modified;
    }
    
    public Score getScore ()
    {
    	return grille.getScore();
    }
    
    public void setScore (int score)
    {
    	 grille.setScore(new Score ());
    }
}
