package controller;

import java.awt.Panel;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import view.GraphicWorld;

public class Event implements MouseListener, MouseMotionListener{

	private GraphicWorld gw;
	private JeuController JeuControler;
	
	public Event (GraphicWorld gw, JeuController jeu){
		setGw(gw);
		JeuControler = jeu;
	}
    
	
    // gestion des événements souris
    public void mousePressed(MouseEvent e) { 
        // on appuie sur le bouton de la souris : récupérer les coordonnées de la première case
    	gw.setSelected(e.getX() / 32, e.getY() / 32);
    	System.out.println("Presse " + gw.getSelectedX()+ " " + gw.getSelectedY());
        gw.repaint();
    }
	
    public void mouseMoved(MouseEvent e ) { 
        // on bouge la souris : récupérer les coordonnées de la deuxième case
        if(gw.ValidateSelected()) {
            gw.setSwapped(e.getX() / 32, e.getY() / 32);
            System.out.println("Move " + gw.getSwappedX()+ " " + gw.getSwappedY());
            // si l'échange n'est pas valide, on cache la deuxième case
            if(!JeuControler.isValidSwap(gw.getSelectedX(), gw.getSelectedY(), gw.getSwappedX(), gw.getSwappedY())) {
                gw.setSwapped(-1, -1);
                System.out.println("non valide");
            }
        }
        gw.repaint();
    }
    
    public void mouseReleased(MouseEvent e ) {
        // lorsque l'on relâche la souris il faut faire l'échange et cacher les cases
        if(gw.ValidateSelected () && gw.ValidateSwapped()) {
        	JeuControler.swap(gw.getSelectedX(), gw.getSelectedY(), gw.getSwappedX(), gw.getSwappedY());
        }
        gw.setSelected(-1, -1);
        gw.setSwapped(-1, -1);
        gw.repaint();
    }

	public GraphicWorld getGw() {
		return gw;
	}

	public void setGw(GraphicWorld gw) {
		this.gw = gw;
	}


	@Override
	public void mouseDragged(MouseEvent e) {
		// TODO Auto-generated method stub
		mouseMoved(e);
	}


	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	
}
