package controller;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import view.GraphicWorld;
import view.MainView;

public class EventMain implements MouseListener, MouseMotionListener {

	private MainView mv;
	
	 public EventMain (MainView mv){
		setMv(mv);
	}
	
	@Override
	public void mouseDragged(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	   // gestion des événements souris
    public void mousePressed(MouseEvent e) { 
        // on appuie sur le bouton de la souris : récupérer les coordonnées de la première case
    	mv.setSelected(e.getX(), e.getY() );
        mv.repaint();
    }

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	public MainView getMv() {
		return mv;
	}

	public void setMv(MainView mv) {
		this.mv = mv;
	}
	
	

}
