package application;

import java.awt.Frame;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;


public class Main {
	
    // met le jeu dans une fenêtre
    public static void main(String args[]) {
        Frame frame = new Frame("Miam, des bonbons !");
        
        
        
        final Appli appli = new Appli();
        
        frame.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent event) {
                System.exit(0);
            }
        });
        //frame.add(new MainView());
        frame.add(appli.gw);
        frame.pack();
        frame.setVisible(true);
    }

}
