package application;

import java.awt.Panel;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import controller.Event;
import controller.JeuController;
import view.GraphicWorld;

public class Appli implements Runnable{
	
	GraphicWorld gw = new GraphicWorld ();
	JeuController jeu = new JeuController ();
	Event event = new Event (gw, jeu);
	
	
	public  Appli ()
	{
		//System.out.println("je commence");
        // remplir une première fois la grille
        while(jeu.fill());
        // enlever les alignements existants
        
        
        while(jeu.removeAlignments()) {
        	
        	//System.out.println("while");
            jeu.fill();
        }
        
        jeu.setScore(0);
        
        gw.addMouseListener(event);
        gw.addMouseMotionListener((MouseMotionListener)event);
        new Thread(this).start();
	}
	
    // boucle principale
    public void run() {
        while(true) {
            // un pas de simulation toutes les 100ms
            try { Thread.currentThread().sleep(100); } catch(InterruptedException e) { }

            // s'il n'y a pas de case vide, chercher des alignements
            if(!jeu.fill()) {
                jeu.removeAlignments();
            }

            // redessiner
            gw.repaint();
        }
    }



}
