package model;


public class BonbonRayeHorizontal extends BonbonState{
	
	private int points = 100;
	
	@Override
	public void action(Grille grille, int x, int y) {
		grille.getBonbons()[x][y].setState(new BonbonBasic());
		for (int i = 0; i < grille.getWidth(); i++) {
			grille.getBonbons()[i][y].getState().action(grille, i, y);
			points += grille.getBonbons()[i][y].getState().getPoints();
		}
	}

	@Override
	public int getPoints() {
		return points;
	}

}
