package model;

public class BonbonMalus extends BonbonState{

	private final int points = -25;
	
	@Override
	public void action(Grille grille, int x, int y) {
		grille.getBonbons()[x][y].setState(new BonbonBasic());
		grille.getBonbons()[x][y].setCouleur(Couleur.White);
	}

	public int getPoints() {
		return points;
	}

}
