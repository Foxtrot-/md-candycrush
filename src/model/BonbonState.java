package model;

public abstract class BonbonState {
		
	public abstract void action (Grille grille, int x, int y);
	public abstract int getPoints(); 

}
