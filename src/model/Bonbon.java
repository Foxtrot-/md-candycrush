package model;

public class Bonbon {

	private Couleur couleur;
	private BonbonState state;

	public Bonbon(Couleur couleur, BonbonState state) {
		super();
		this.couleur = couleur;
		this.state = state;
	}
	public Couleur getCouleur() {
		return couleur;
	}
	public void setCouleur(Couleur couleur) {
		this.couleur = couleur;
	}
	public BonbonState getState() {
		return state;
	}
	public void setState(BonbonState state) {
		this.state = state;
	}
	
}
