package model;


public class BonbonRayeVertical extends BonbonState{
	
	private int points = 100;
	
	@Override
	public void action(Grille grille, int x, int y) {
		grille.getBonbons()[x][y].setState(new BonbonBasic());
		for (int j = 0; j < grille.getWidth(); j++) {
			grille.getBonbons()[x][j].getState().action(grille, x, j);
			points += grille.getBonbons()[x][j].getState().getPoints();
		}
	}

	@Override
	public int getPoints() {
		return points;
	}

}
