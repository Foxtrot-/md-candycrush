package model;

public class Score {

	private int score ;
				
		public Score(){
			score = 0;
		}
		
		public Score( int sc ){
			score = sc;
		}
		
		/** return le score	*/
		public int getPoints(){
			return score;
		}
		
		/** Ajoute le nombre de point au score	*/
		public void add(int points){
			score += points;
		}
	
	
}
