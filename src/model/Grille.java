package model;

import util.Utils;

public class Grille {
	
	private Bonbon[][] bonbons;
	private Score score ;

	private int height;
	private int width;
	
	private static Grille INSTANCE = null;
	
	private Grille() {
		init(8,8);
		score = new Score();
	}
	
	
	public static Grille getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new Grille();
		}
		return INSTANCE;
	}
	
	/*initialiser la grille */
	
	public void init(int height, int width){
		bonbons =   new Bonbon [width][height];
		
		for (int y = 0; y< height; y++) {
			for (int x = 0; x < width; x++) {
				newBonbon(x,y);
				//addBonbon(x,y,color);
			}
		}
		
		this.height = height;
		this.width = width;
	}
	
	
	/*supprimer un bonbon*/
	
	/*public void removeBonbon(int x ,int y) {
		moveDown(x,y);
		bonbons[x][y-1].setCouleur(Couleur.White);
	}*/
	
	/*ajout d'un bonbon*/
	
	/*public void addBonbon(int x, int y, Couleur color) {
		if (color == null)
			color = Utils.getRandomColor();
		bonbons[x][y] = new Bonbon(color,new BonbonBasic());
	}*/

	/*faire descendre les bonbons plus haut */
	public void moveDown(int x, int y) {
		bonbons[x][y].setCouleur( bonbons[x][y-1].getCouleur());
        bonbons[x][y].setState( bonbons[x][y-1].getState());
        bonbons[x][y-1].setCouleur(Couleur.White);
		
	}
	
	// échanger le contenu de deux cases
	public void swap(int x1, int y1, int x2, int y2) {
        Couleur tmp = bonbons[x1][y1].getCouleur();
        bonbons[x1][y1].setCouleur(bonbons[x2][y2].getCouleur()); ;
        bonbons[x2][y2].setCouleur(tmp);
    }
    
    
    // est-ce qu'on a trois cases de la même couleur vers le droite depuis (i, j) ?
    public boolean horizontalAligned(int i, int j) {
        if(i < 0 || j < 0 || i >= width-2 || j >= height) 
        	return false;
        
        if(bonbons[i][j].getCouleur().equals(bonbons[i + 1][j].getCouleur()) && bonbons[i][j].getCouleur().equals(bonbons[i + 2][j].getCouleur())) 
        	return true;
        
        return false;
    }

    // est-ce qu'on a trois cases de la même couleur vers le bas depuis (i, j) ?
    public boolean verticalAligned(int i, int j) {
        if(i < 0 || j < 0 || i >= width || j >= height-2) 
        	return false;
        if(bonbons[i][j].getCouleur().equals(bonbons[i][j + 1].getCouleur()) && bonbons[i][j].getCouleur().equals(bonbons[i][j + 2].getCouleur())) 
        {
        	return true;
        }
        return false;
    }
    
    // supprimer les alignements
    public boolean removeAlignments() {
    	boolean [][]marked = new boolean [width][height];
    	
        // passe 1 : marquer tous les alignements
        for(int i = 0; i < width; i++) {
            for(int j = 0; j < height; j++) {
                if(!bonbons[i][j].getCouleur().equals(Couleur.White) && horizontalAligned(i, j)) {
                    marked[i][j] = marked[i + 1][j] = marked[i + 2][j] = true;
                }
                if(!bonbons[i][j].getCouleur().equals(Couleur.White) && verticalAligned(i, j)) {
                    marked[i][j] = marked[i][j + 1] = marked[i][j + 2] = true;
                }
            }
        }
        
        // passe 2 : supprimer les cases marquées
        boolean modified = false;
        for(int i = 0; i < width; i++) {
            for(int j = 0; j < height; j++) {
                if(marked[i][j]) {
                	
                	bonbons[i][j].getState().action(this, i, j);
                	score.add(bonbons[i][j].getState().getPoints());
//                	bonbons[i][j].setState(new BonbonBasic());
                    modified = true;
                }
            }
        }
        return modified;
    }
    
	public Bonbon[][] getBonbons() {
		return bonbons;
	}

	public void setBonbons(Bonbon[][] bonbons) {
		this.bonbons = bonbons;
	}
	

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}


	public void newBonbon(int i, int j) {
		Couleur color = Utils.getRandomColor();
    	if (Utils.isMystere())
    		bonbons[i][j] = new Bonbon (color, new BonbonMystere());
    	else if (Utils.isRayeHorizontal())
    		bonbons[i][j] = new Bonbon (color, new BonbonRayeHorizontal());
    	else if (Utils.isRayeVertical())
    		bonbons[i][j] = new Bonbon (color, new BonbonRayeVertical());
    	else if (Utils.isBombe())
    		bonbons[i][j] = new Bonbon (color, new BonbonBombe());
    	else if (Utils.isMalus())
    		bonbons[i][j] = new Bonbon (color, new BonbonMalus());
		else 
			bonbons[i][j] = new Bonbon (color, new BonbonBasic());
		
	}

	public Score getScore() {
		return score;
	}


	public void setScore(Score score) {
		this.score = score;
	}
}
