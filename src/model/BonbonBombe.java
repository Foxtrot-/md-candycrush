package model;

public class BonbonBombe extends BonbonState{
	
	private int points = 100;
	
	@Override
	public void action(Grille grille, int x, int y) {
		grille.getBonbons()[x][y].setState(new BonbonBasic());
		
		for (int i = x-1; i <= x+1; i++) {
			if (i < 0 || i >= grille.getWidth())
				continue;
			
			for (int j = y-1; j <= y+1; j++) {
				if (j < 0 || j >= grille.getHeight())
					continue;
				
				grille.getBonbons()[i][j].getState().action(grille, i, j);
				points += grille.getBonbons()[i][j].getState().getPoints();
				
			}
		}
	}

	@Override
	public int getPoints() {
		return points;
	}

}
