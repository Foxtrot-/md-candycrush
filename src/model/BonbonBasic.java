package model;

public class BonbonBasic extends BonbonState{

	private final int points = 50;
	
	@Override
	public void action(Grille grille, int x, int y) {
		grille.getBonbons()[x][y].setCouleur(Couleur.White);
	}

	public int getPoints() {
		return points;
	}

}
