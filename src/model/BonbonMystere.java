package model;

public class BonbonMystere extends BonbonState{

	private final int points = 60;
	
	@Override
	public void action(Grille grille, int x, int y) {
		grille.getBonbons()[x][y].setState(new BonbonBasic());
		grille.getBonbons()[x][y].setCouleur(Couleur.Green);
	}
	
	public int getPoints() {
		return points;
	}

}
