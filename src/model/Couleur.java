package model;

import java.awt.Color;

public enum Couleur {
	Purple,
	Red,
	Blue,
	Yellow,
	Green,
	White;

	public static Color getColor(Couleur c)
	{
		switch (c){
		case Purple:
			return Color.MAGENTA;
		case Red:
			return Color.RED;
		case Blue:
			return Color.BLUE;
		case Yellow:
			return Color.YELLOW;
		case Green:
			return Color.GREEN;
		
		case White:
		return Color.WHITE;
	}
		return null;
	}
}
